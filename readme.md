# Nix Plop

## What is it?

This is a wrapped version of [`Plop.js`](https://plopjs.com/documentation/)
which is a small tool designed to generate source files from templates. It's
cool. Use it for team projects. I did this so I could use plop in no Node.js
projects. I didn't want extra node related files cluttering the project. With
this, you can load the `plop` tool as a development shell dependency. It's
isolated in the background.

## How do I install it?

Here is a minimal example project flake that embeds `plop` in your development
shell.

```nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    systems.url = "github:nix-systems/default";
    nix-plop.url = "gitlab:cbleslie/nix-plop"; # This is the repo.
  };

  outputs = {
    systems,
    nixpkgs,
    nix-plop, # or use inputs.nix-plop if you want.
    ...
  }: let
    forAllSystems = nixpkgs.lib.genAttrs (import systems);
  in {
    devShells = forAllSystems (system: {
      default = nixpkgs.legacyPackages.${system}.mkShell {
        buildInputs = with nixpkgs.legacyPackages.${system};
          [
            nix-plop.packages.${system}.default # Call directly from input flake.
          ];
      };
    });
  };
}
```

## How do I run it?

type `plop`, make sure you have a `plopfile` and some templates to scaffold.

## Gotchas

Instead of `plopfile.js` use `plopfile.mjs` because this doesn't ship with it's
own `package.json`.
