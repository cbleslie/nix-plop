{
  inputs = {
    systems.url = "github:nix-systems/default";
  };

  outputs = {
    systems,
    nixpkgs,
    ...
  }: let
    forAllSystems = nixpkgs.lib.genAttrs (import systems);
  in {
    packages = forAllSystems (system: let
      pkgs = import nixpkgs {inherit system;};
      meta = rec {
        pname = "plop";
        version = "4.0.1";
        name = "${pname}@${version}";
      };
    in rec {
      default = plop;
      plop = pkgs.writeShellApplication {
        name = "${meta.pname}";
        runtimeInputs = with pkgs; [nodejs];
        text = ''
          npx --yes ${meta.name} "$@"
        '';
      };
    });
  };
}
